# ISO/IEEE 11073 resources

This package repackages the FHIR CodeSystem for the ISO/IEEE 11073 standard. The original FHIR CodeSystem is available in the RTMMS system at https://rtmms.nist.gov/.